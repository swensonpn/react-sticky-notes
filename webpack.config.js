module.exports = {
  entry: { main: "./src/lib" },
  output: {
    path: __dirname + "/dist",
    filename: "react-sticky-notes.js",
    libraryTarget: "umd",
    library: "react-sticky-notes",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: "babel-loader" },
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ],
  },
  externals: {
    react: "react"
  },
};
