import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './demo';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();

// Put the thing into the DOM!
//ReactDOM.render(<App />, document.getElementById('root'));
