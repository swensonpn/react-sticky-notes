import React, { Component } from 'react';
import { Board, LaneGroup, Lane, Card } from '../lib';

const initCardCount = 10;

// fake data generator
const getItems = count =>
  Array.from({ length: count }, (v, k) => k).map(k => ({
    id: `item-${k}`,
    type:"backlog" ,
    content: `item ${k}`,
    story:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    color:"rgb(255,247,64)"
  }));

const groupBy = (groupByProperty, itemsIn = [], itemsOut = {}) => {
  return itemsIn.reduce((grouped, item) => {
    // since groups are not optional maybe we dont' create
    if(grouped[item[groupByProperty]]) {
      grouped[item[groupByProperty]].push(item);

    }
    return grouped;
  }, itemsOut);
};

const move = (items, destId, destIndex, srcId, scrIndex) => {
  const srcList = Array.from(items[srcId]),
        destList = (srcId === destId) ? srcList : Array.from(items[destId]);

  const [item] = srcList.splice(scrIndex, 1);

  item.type = destId;
  destList.splice(destIndex, 0, item);


  items[destId] = destList;
  items[srcId] = srcList;

  return items;
};

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: null
    };

    this._lastId = null;
  }

  /* Do Ajax call here */
  componentWillMount() {
    const allItems = getItems(initCardCount);

    this._lastId = initCardCount - 1;
    this.setState({
      items: groupBy("type", allItems, {
        backlog:[],
        ready:[],
        working:[],
        complete:[]
      })
    });
  }

  handleAddCard(laneId) {
    if(this.state.items[laneId]) {
      this._lastId++;

      this.state.items[laneId].push({
        id: `item-${this._lastId}`,
        type:laneId ,
        content: `item ${this._lastId}`,
        story:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
        color:"rgb(255,247,64)"
      });

      this.setState(this.state.items);
    }
  }

  handleCardClick(id, rectangle) {
    console.log(id);
    console.log(rectangle);
  }

  handleMove(evt) {
    if(typeof evt === "object") {
      this.setState({
        items: move(this.state.items, evt.destination.laneId, evt.destination.index, evt.source.laneId, evt.source.index)
      });
    }
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {console.log("render")
    const items = this.state.items;

    return (
      <Board label="Personal Kanban" onDragEnd={ this.handleMove.bind(this) } listPropertyName="type">
        <LaneGroup>
          <Lane label="Backlog" laneId="backlog"  onAddCard={ this.handleAddCard.bind(this) }>
            {items.backlog.map((item, index) => (
              <Card key={item.id} draggableId={item.id} index={index} color={item.color} handleClick={ this.handleCardClick.bind(this) }>{item.story}</Card>
            ))}
          </Lane>
          <LaneGroup label="Doing" width={ 3/4 } orientation="vertical">
            <Lane label="Ready" laneId="ready">
              {items.ready.map((item, index) => (
                  <Card key={item.id} draggableId={item.id} index={index} color={item.color}>{item.story}</Card>
              ))}
            </Lane>
            <Lane label="Working" laneId="working">
              {items.working.map((item, index) => (
                 <Card key={item.id} draggableId={item.id} index={index} color={item.color}>{item.story}</Card>
             ))}
            </Lane>
            <Lane label="Done" laneId="complete">
              {items.complete.map((item, index) => (
                  <Card key={item.id} draggableId={item.id} index={index} color={item.color}>{item.story}</Card>
              ))}
            </Lane>
          </LaneGroup>
        </LaneGroup>
      </Board>
    );
  }
}
