import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import './card.css';

const getStyles = (isDragging, draggableStyle, color) => {
  const styles = {
    backgroundColor: (color) ? color : "rgb(255,247,64)",
    opacity: isDragging ? .5 : 1
  };

  // styles we need to apply on draggables
  for(let n in draggableStyle) {
    styles[n] = draggableStyle[n];
  }

  return styles;
};

export class Card extends Component {

  handleClick(e) {
    if(typeof this.props.handleClick === 'function') {
      this.props.handleClick(this.props.draggableId, e.target.getBoundingClientRect());
    }
  }

  render() {

    return(
      <Draggable draggableId={ this.props.draggableId } index={ this.props.index }>
        {(provided, snapshot) => (
          <div
            className={ "sticky-note-card" + ((snapshot.isDragging) ? " dragging" : "") }
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={getStyles(
              snapshot.isDragging,
              provided.draggableProps.style,
              this.props.color
            )}
            onClick={ this.handleClick.bind(this) }
          >
            {this.props.children}
          </div>
        )}
      </Draggable>
    );
  }
}
