import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import './board.css'

const getEventObject = (result) => {
  const evt = {
    cardId: result.draggableId,
    source: {
      laneId: result.source.droppableId,
      index: result.source.index
    }
  };

  if(result.destination) {
    evt.destination = {
      laneId: result.destination.droppableId,
      index: result.destination.index
    };
  }

  return evt;
}

export class Board extends Component {

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    if(typeof this.props.onDragEnd === 'function') {
      this.props.onDragEnd(getEventObject(result));
    }
  }

  onDragStart(result) {
    if(typeof this.props.onDragStart === 'function') {
      this.props.onDragStart(getEventObject(result));
    }
  }

  render() {
    // const childSizes = React.Children.map(this.props.children, (child) => {
    //   return child.props.width || child.props.height;
    // });

    const context = ((children) => {
      if(children) {
        return(
          <DragDropContext onDragEnd={ this.onDragEnd.bind(this) } onDragStart={ this.onDragStart.bind(this) }>
            { this.props.children }
          </DragDropContext>
        );
      }
    })(this.props.children);

    return(
      <div className="sticky-note-board">
        <strong>{ this.props.label }</strong>
        <div>
          { context }
        </div>
      </div>
    );
  }
}
