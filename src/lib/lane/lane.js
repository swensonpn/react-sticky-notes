import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import './lane.css';

export class Lane extends Component {

  handleAddCard(e) {
    this.props.onAddCard(this.props.laneId);
  }

  render() {
    const addCardButton = (typeof this.props.onAddCard === 'function') ?
      <button type="button" title="add new card" onClick={ this.handleAddCard.bind(this) }><span>&#x2b;</span></button> : "";

    const className = "sticky-note-lane" + ((this.props.parentOrientation === "vertical") ? " horizontal" : "");

    return (
      <div className={ className }>

        { addCardButton } <strong><span>{ this.props.label }</span></strong>
        <Droppable droppableId={ this.props.laneId }>
            {(provided, snapshot) => (
              <div ref={ provided.innerRef } className={ snapshot.isDraggingOver ? "dragover" : "" }>
                { this.props.children }
                {provided.placeholder}
              </div>
            )}
        </Droppable>
      </div>
    );
  }
}
