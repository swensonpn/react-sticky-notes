import React, { Component } from 'react';
import './lane_group.css';

const ORIENTATIONS = ["horizontal","vertical"],
      DEFAULT_ORIENTATION = ORIENTATIONS[0];

const getCSSGridValues = (childSizes) => {
  let remainder = 0,
      autoChildren = [],
      autoChildWidth = 0,
      values = "";

  for(let i=0; i<childSizes.length; i++) {
    if(childSizes[i] === "auto") {
      autoChildren.push(i);
    } else {
      remainder += childSizes[i];
    }
  }

  if(remainder === 0) {
    autoChildWidth = 1/childSizes.length;
  } else if( remainder < 1) {
    autoChildWidth = 1 - remainder;
  }

  for(let i=0; i<autoChildren.length; i++) {
    childSizes[autoChildren[i]] = autoChildWidth;
  }

  for(let i=0; i < childSizes.length; i++) {
    values += (childSizes[i] * 100) + "% ";
  }

  return (values.length === 0) ? "" : values.slice(0,-1);
}

export class LaneGroup extends Component {

  componentWillMount() {
    if(ORIENTATIONS.indexOf(this.props.orientation) > -1) {
      this._orientation = this.props.orientation
    } else {
      this._orientation = DEFAULT_ORIENTATION;
    }

    const childSizes = React.Children.map(this.props.children, (child) => {
      return ((this._orientation === "horizontal") ? child.props.width : child.props.height) || "auto";
    }) || [];

    const gridSizes = getCSSGridValues(childSizes);

    this._styles = {display:"grid"};
    if(this._orientation === ORIENTATIONS[0]) {
      this._styles.gridTemplateColumns = gridSizes;
    }else {
      this._styles.gridTemplateRows = gridSizes;
    }
  }

  render() {
    const label = (this.props.label) ? <strong>{ this.props.label }</strong> : "";

    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, { parentOrientation:this._orientation})
    });

    return (
      <div className="sticky-note-lane-group">
        { label }
        <div style={ this._styles  }>
          { children }
        </div>
      </div>
    );
  }
}
